// creating a copy array with three different methods

const fruits = ["apple","banana"];
const copyfruit = [...fruits]
console.log(copyfruit+'<br>');

// From method
const newfruits = ["apple","banana","grapes"];
const copyfruits = Array.from(newfruits);
console.log(copyfruits+'<br>');

// Slice method
const slicefruits = ["mango","kiwi"]
const newslice = slicefruits.slice();
console.log(newslice+'<br>')

// Pass JSON formatted data only in JSON parse 
// JSON parse methods returns the object string null boolean data 
// JSON parse method using object
const jsonobj = '{"name": "nirali","Age": 20}'
const obj = JSON.parse(jsonobj);
console.log(obj);

// JSON parse using string
const stringjson = '"Nirali Gosaliya"'
console.log(stringjson);

// JSON parse using array 
const arrayjson = '["Tomato","cucumber","potato"]'
console.log(arrayjson);

// Object keys method
const objkey = ["Jinal","nirali","darsh"]
console.log(Object.keys(objkey))

// find method
// it returns the first element only that satisfies the condition
// if no match found then undefined will be returned 

const findmethod = [3,4,7,8,10,89,56,78,99]
const newval = findmethod.find((element)=>element>10)
console.log(newval);

// findIndex method
const findvalue = [10,20,40,54,45]
const findindex = findvalue.findIndex((element)=>element>20)
console.log(findindex);

// toString() method 
const value = {
    "Name": "Nirali",
    "profile": "developer",
    "tech": "react",
    "age": 20
}

let nameVal = value['Name'];
const values = nameVal.toString();
console.log(typeof values)

// slice method 
const animals = ["camel","duck","ant","bison","elephant"]
console.log(animals.slice(2,-1))

//  valueOf method
// It returns an array itself
const cars = ["maruti","BMW","audi"]
const newcars = cars.valueOf();
console.log(newcars);

// entries() method
// returns the key-value pair in an array form
const flower = ["lilly","lotus","rose"]
const f = flower.entries();
for(let x of f){
    console.log(x)
}

// Switch statement

const pet = "dog";
switch(pet){
    case "lizard":
        console.log("i am lizard");
        break;
    case "dog":
        console.log("dog");
        break;
    case "snake":
        console.log("snake");
        break;

    default: 
       console.log("not found")
       break;
    }

    // for in loop is used to access key . we can also used with object
    // in object for in returns the key
    // in an array for in returns the index
    // for of loop is used for an array
    // we cannot use the for of loop in object

    const student = {
        name: 'Monica',
        class: 7,
        age: 12
    }

    for(let y in student){
        console.log(y +" : "+student[y]);
    }
    // for in loop in string
     const Name="nirali";
     for(let z in Name){
        console.log(z+":"+Name[z])
     }

    //  for of loop in string
    const Namevalue = "akshat";
    for(let u of Namevalue){
        console.log(u);
    }

    // set method stores the unique value
    // it can store any type of value
    // it has many methods like size() ,add(),delete(),clear(),entries(),has(),values()
    var set2 = new Set("fooooooood");
    console.log(set2)

    // add elements to the set object
    const set1 = new Set();
    set1.add(10).add(20);
    console.log(set1)

    // delete()
    // returns true
    var set2 = new Set("foooodiiiieee");
    console.log(set2.delete('e'))
    console.log(set2)

    // clear
    var set3 = new Set([10, 20, 30, 40, 50]);
    set3.clear()
    console.log(set3)

    // Promise method
    function prom (complete){
        return new Promise(function(resolve,reject){
            console.log("fetching the data")
            setTimeout(()=>{
                if(complete){
                    resolve("sucessfully completed")
                }
                else{
                    reject("Rejeted")
                }
            },2000)
        })
    }
    prom(true).then((result)=>{
        console.log(result)
    }).catch((error)=>{
        console.log(error)
    })
    
//    promise method using an array
const promise  = new Promise(function(resolve){
    setTimeout(()=>{
        resolve([1,2,3,4,5,6])
    },[1000])
}).then((values)=>{
    console.log(values[3])
})

// Promise.all() method
// it takes an array as parameter
var p1= Promise.resolve(40);
var p2 = 1818;
var p3 = new Promise((resolve,reject)=>{
    setTimeout(()=>{
        resolve("flower")
    },1000)
})
Promise.all([p1,p2,p3]).then((result)=>{
    console.log(result)
})

// another example of promise.all()
const a1 = Promise.resolve(100)
const a2= new Promise((resolve,reject)=>{
    setTimeout(()=>{
        resolve("I am resolved");
    },1000);
})

Promise.all([a1,a2]).then((values)=>{
    console.log(values);
})

// another example of promise
const b1 = Promise.resolve(12)
const b2 = new Promise((resolve,reject)=>{
    setTimeout(()=>{
        reject(new Error("rejection"))
    },1000)
})
const b3 = new Promise((resolve,reject)=>{
    setTimeout(()=>{
        resolve("Resolved")
    })
})
Promise.all([
    b1.catch((error)=>{
        return error
    }),
   b2.catch((error)=>{
        return error
    }),
    b3.catch((error)=>{
        return error
    })
]).then((values)=>{
    console.log(values[0])
    console.log(values[1])
    console.log(values[2])
})

// OOPS Concept

let info = {
    'firstName': "Nirali",
    lastName: "Gosaliya",

    getFunction: function(){
        return(`${info.firstName} ${info.lastName}`)
    },
    phone: {
        person_phone: 9274659870
    }

}
console.log(info.getFunction())
console.log(info.phone);
console.log(info.phone.person_phone)
console.log(info['first Name'])

// encapsulation example
class Person{
    constructor(name,id){
        this.name=name,
        this.id = id
    }
    add_address(add){
        this.add = add
    }
    getDetails(){
        console.log(`Name is ${this.name} and address is ${this.add}`)
    }
}

let person1 = new Person('Mukul',21)
person1.add_address("Delhi");
person1.getDetails();

// Object methods
// assign method
// syntax Object.assign(target,source)
// it returns the target object

const target = {a:1,b:2}
const source = {c: 3,d:5}

const assignmethod = Object.assign(target,source)
// console.log(assignmethod) write this also produce the same result
console.log(target)

// Object freeze method
// it prevents the modification of an existing property value
// it prevents the new property that being addded to it
const obj1 = {
    props1: 30
}
const obj2 = Object.freeze(obj1)
obj2.props1 = 50
// return 30
console.log(obj2.props1)

// Object.seal() prevents the new property from being added to it
// we can modify the existing property value

var information ={
    name: "Nirali"
}
var newseal = Object.seal(information)
console.log(newseal)
newseal.name="Carry"
console.log(newseal.name)

// cannot delete when sealed
var c1 ={
    name: "Darsh"
}
Object.seal(c1)
c1.name = "Anisha"
console.log(c1.name)
delete c1.name

// Object.values() returns the array of property values
const d1 = {
    profile: "developer",
    'experience': '2 years'

}
const d2 = Object.values(d1)
console.log(d1.experience)

//Javascript DOM
function changeColor(value){
    let newval = document.getElementById('colorvalue');
    newval.style.color=value
}
// Input events
// Onchange ,Onblur and input
// let x = document.getElementById('myinput');
// x.addEventListener('focus',handleClick)
// x.addEventListener('blur',handleBlur)
// x.addEventListener('input',handleInput)

function handleClick(){
    x.style.backgroundColor= "yellow"
}
function handleBlur(){
    x.style.backgroundColor="cyan"
}
function handleInput(){
    console.log(this.value)
}

// dblClick method

// let y = document.getElementById('demo');
// y.addEventListener('dblclick',fun)
//  function fun(){
//      document.getElementById('demo').innerHTML="welcome"
//  }

// //  onresize method
// let z = document.getElementById('resize_examp');
// z.addEventListener('resize',myresize)
// function myresize(){
//     var w = window.outerWidth;
//     var h = window.outerHeight;
//    console.log(w);
//    console.log(h);
// }

// Lexicals cope in closure
