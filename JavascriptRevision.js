
// Lexical Scope
const myfunction = () => {
    let value = 3
    console.log(value)

     const childrenFunction = () => {
         console.log(value+2)
     } 
     childrenFunction()
}
myfunction()


// closure 
// closure is a function having access to the parent scope even after the parent functin has closed
const myfunction2 = () => {
    let value = 3
    console.log(value)

     const childrenFunction = () => {
         console.log(value+2)
     } 
     return childrenFunction
}
let result = myfunction2()
console.log(result)

// Call method
// used to borrow function

let personDetail = {
    firstname: "nirali",
    lastname: "gosaliya",
  
}
let fullname = function(state,position){
    console.log(this.firstname+" "+this.lastname+" "+state+" "+position)
}
fullname.call(personDetail,"Banglore","Developer")

let personDetailBorrow = {
    firstname: "Jinal",
    lastname: "gosaliya"
}
fullname.call(personDetailBorrow,"Hydrabad","Backend developer")


// apply method
// takes an arguments as an array
fullname.apply(personDetailBorrow,["Hydrabad","Backend developer"])


// bind method make a copy of a method insteadof directly calling the method
let bindmethodexample = fullname.bind(personDetail,"Banglore","Developer")
bindmethodexample();

// javascript objects are mutable and reference type
const person = {
    firstname: "Darsh",
    lastname: "Shah",
    age: 21,
    eyeColor: "blue"
}

let x = person
x.eyeColor="green"
console.log(person.firstname+" "+person.lastname+" "+"eyecolor"+" "+"is"+" "+person.eyeColor)

// javascript Iterators
let numbers = [1000,2000,3000]
console.log(typeof numbers[Symbol.iterator])
let y = numbers[Symbol.iterator]();
console.log(y)
console.log(y.next())


// Anagrams Task

// input : strs = ['eat','tea','tan','ate','nat','bat']
// output : [['bat'],['nat','tan'],["ate",'eat','tea']]

var personName = "Nirali is a girl"
var res= personName.split(" ")
console.log(res)


const strs = ['eat','tea','tan','ate','nat','bat']
const emptyArray = (strs = []) => {
    if(strs.length === 0){
        return strs;
    }
    const map = new Map();
    for(let str of strs){
        console.log("Str => ",str)//eat
        const sorted = [...str]//['e','a','t']
        console.log("Sorted array" ,sorted)
        console.log(sorted.sort().join(''))
        console.log("condition",map.has(sorted))
        if(map.has(sorted.sort().join(''))){
            map.get(sorted.sort().join('')).push(str)
        }
        else{
            map.set(sorted.sort().join(''),[str])
        }
    }
    return [...map.values()]
}
console.log(emptyArray(strs).sort(function(a,b){return a.length-b.length}))
